﻿using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Api
{
    public class ServiceCollection
    {
        public ContactService ContactService { get; set; }
        public MessageService MessageService { get; set; }
        public RepositoryCollectionBase RepositoryCollection { get; set; }
        public ServiceCollection()
        {
            var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();

            bool memoryRepos = true;
            if(bool.TryParse(config["MemoryRepos"], out memoryRepos))
            {
                if(!memoryRepos)
                {
                    string connectionStr = config["ConnectionStrings:db"];
                    var contextOptions = new DbContextOptionsBuilder().UseSqlServer(connectionStr).Options;
                    var efcontext = new EntityFramework.MySimpleMessageServiceContext(contextOptions);
                    RepositoryCollection = Repositories.RepositoryCollectionFactory.GenerateEFRepository(efcontext);
                }
                else
                {
                    RepositoryCollection = Repositories.RepositoryCollectionFactory.GenerateMemoryRepositoryCollection();
                }
                InstaniateServices();
            }
        }

        private void InstaniateServices()
        {
            Common.ServiceCollection collection = new Common.ServiceCollection() { ContactService = this.ContactService, MessageService = this.MessageService, RepositoryCollection = this.RepositoryCollection };
            this.ContactService = new ContactService(collection);
            this.MessageService = new MessageService(collection);
        }
    }
}
