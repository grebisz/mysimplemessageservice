using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.EntityFramework;
using Microsoft.AspNetCore.Http;
using System.Net;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;

namespace Acme.MySimpleMessageService.Api.Controllers
{
    [ApiController]
    [Route("{contactId}/[controller]")]
    public class MessagesController : ControllerBase
    {
        MessageService messageService { get; set; }
        ContactService contactService { get; set; }
        public MessagesController(ServiceCollection serviceCollection)
        {
            messageService = serviceCollection.MessageService;
            contactService = serviceCollection.ContactService;
        }

        [HttpGet]
        public ActionResult<RequestResult<Message>> Get(Guid contactId, MessageStatus messageStatus = MessageStatus.SENT, int page = 0, int pageSize = 10)
        {
            var result = messageService.GetMessages(contactId, page: page, pageSize: pageSize, messageStatus: messageStatus);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpDelete("{id}")]
        public ActionResult<RequestResult<Message>> Delete(Guid id)
        {
            var result = messageService.DeleteMessage(id);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpPost("MessageByName")]
        public ActionResult<RequestResult<Message>> SendMessageByName(string toName, string message)
        {
            var result = messageService.SendMessage(contactService.GetById((Guid)RouteData.Values["contactId"]).Username, toName, message);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpPost("MessageById")]
        public ActionResult<RequestResult<Message>> SendMessageById(Guid toid, string message)
        {
            var result = messageService.SendMessage((Guid)RouteData.Values["contactId"], toid, message);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpPost("{id}/read")]
        public void MarkAsRead(Guid id)
        {
            messageService.MarkMessageAsRead(id);
        }
    }
}
