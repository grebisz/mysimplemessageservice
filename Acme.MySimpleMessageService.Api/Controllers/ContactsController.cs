using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.EntityFramework;
using Microsoft.AspNetCore.Http;
using System.Net;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.Common.Enums;

namespace Acme.MySimpleMessageService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactsController : ControllerBase
    {
        ContactService contactService { get; set; }
        public ContactsController(ServiceCollection serviceCollection)
        {
            contactService = serviceCollection.ContactService;
        }

        [HttpGet]
        public ActionResult<RequestResult<Contact>> Get(int page = 0, int pageSize = 10)
        {
            var result = contactService.Get(page, pageSize);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpDelete("{id}")]
        public ActionResult<RequestResult<Contact>> Delete(Guid id)
        {
            var result = contactService.Delete(id);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }

        [HttpPut("{id}")]
        public ActionResult<RequestResult<Contact>> Update(Guid id, [FromBody] Contact contact)
        {
            var result = contactService.Update(id, contact);
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }


        [HttpPost]
        public ActionResult<RequestResult<Contact>> Create(string userName)
        {
            var result = contactService.Create(new Contact()
            {
                Username = userName,
                ContactStatus = ContactStatus.OFFLINE,
                ContactStatusMessage = ""
            });
            Response.StatusCode = (int)result.HttpStatusCode;
            return result;
        }
    }
}
