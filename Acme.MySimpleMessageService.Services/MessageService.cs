﻿using Acme.MySimpleMessageService.Repositories.Interfaces;
using Acme.MySimpleMessageService.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Acme.MySimpleMessageService.Common.Common.Interfaces.Services;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;

namespace Acme.MySimpleMessageService.Common
{
    public class MessageService : IMessageService
    {
        //Dependent Services
        IContactService ContactService;
        //The repo this service owns
        IRepository<Guid, Message> MessageRepo;
        
        public MessageService(ServiceCollection serviceCollection)
        {
            ContactService = serviceCollection.ContactService;
            MessageRepo = serviceCollection.RepositoryCollection.MessageRepository;
        }

        public RequestResult<Message> DeleteMessage(Guid messageId, bool hardDelete = false)
        {
            RequestResult<Message> result;
            try
            {
                var obj = MessageRepo.GetById(messageId);
                if (obj != null)
                {
                    if(hardDelete)
                    {
                        MessageRepo.Delete(messageId, true);
                    }
                    else
                    {
                        MessageRepo.Update(obj, new Message()
                        {
                            DateDeleted = DateTime.UtcNow,
                            Status = MessageStatus.DELETED,
                            DateRead = obj.DateRead,
                            ReceiverId = obj.ReceiverId,
                            SenderId = obj.SenderId,
                            MessageText = obj.MessageText,
                            DateSent = obj.DateSent
                        }, true);
                    }
                    result = RequestResultFactory.Generate<Message>();
                    result.Results = new List<Message>() { obj };
                }
                else
                {
                    result = RequestResultFactory.GenerateNotFound<Message>();
                }
            }
            catch(Exception ex)
            {
                result = RequestResultFactory.GenerateException<Message>(ex);
            }
            return result;
        }

        public RequestResult<Message> GetMessages(Guid contactId, 
            int page=0, int pageSize=10, MessageStatus messageStatus = MessageStatus.DEFAULT,
            DateTime? fromDateFilter = null, DateTime? toDatefilter = null, 
            string sortBy="DateSent", bool sortAsc=false)
        {
            return (MessageRepo as IMessageRepository).GetMessages(contactId, page, pageSize, messageStatus, fromDateFilter, toDatefilter, sortBy, sortAsc);
        }

        public void MarkMessageAsRead(Guid messageId)
        {
            var message = MessageRepo.GetById(messageId);
            if(message != null)
            {
                MessageRepo.Update(message, new Message()
                {
                    DateSent = message.DateSent,
                    ReceiverId = message.ReceiverId,
                    SenderId = message.SenderId,
                    Status = MessageStatus.READ,
                    DateRead = DateTime.UtcNow
                }, true);
            }
        }

        public RequestResult<Message> SendMessage(Guid fromUser, Guid toUser, string message)
        {
            RequestResult<Message> result = null;
            try
            {
                if (fromUser == Guid.Empty || toUser == Guid.Empty)
                {
                    result = RequestResultFactory.GenerateNotFound<Message>();
                    if (fromUser == Guid.Empty) result.AdditionalInfo += $"Sender could not be identified\n";
                    if (toUser == Guid.Empty) result.AdditionalInfo += $"Receiver could not be identified\n";
                }
                if (String.IsNullOrEmpty(message))
                {
                    result = RequestResultFactory.GenerateArguementError<Message>("message", "Message must contain a value");
                }
                var msg = new Message()
                {
                    DateSent = DateTime.UtcNow,
                    ReceiverId = toUser,
                    SenderId = fromUser,
                    MessageText = message
                };
                MessageRepo.Insert(msg, true);
                result = RequestResultFactory.Generate<Message>();
                result.Results = new List<Message>() { msg };
            }
            catch (Exception ex)
            {
                result = RequestResultFactory.GenerateException<Message>(ex);
            }
            return result;
        }

        public RequestResult<Message> SendMessage(string fromName, string toName, string message)
        {
            return SendMessage(ContactService.GetIdForUser(fromName), ContactService.GetIdForUser(toName), message);
        }

        public RequestResult<Message> SendMessage(Contact from, Contact to, string message)
        {
            return SendMessage(from.Id, to.Id, message);
        }
    }
}
