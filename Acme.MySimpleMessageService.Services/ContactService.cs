using System;
using System.Collections.Generic;
using System.Linq;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Acme.MySimpleMessageService.Common.Interfaces;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;

namespace Acme.MySimpleMessageService.Common
{
    public class ContactService : IContactService
    {
        IRepository<Guid, Contact> repo;
        IContactRepository contactRepo;

        Dictionary<Guid, string> usernameCache;

        public ContactService(ServiceCollection collection)
        {
            this.repo = collection.RepositoryCollection.ContactRepository;
            this.contactRepo = (IContactRepository)collection.RepositoryCollection.ContactRepository;
            usernameCache = new Dictionary<Guid, string>();
        }

        public RequestResult<Contact> Create(Contact contact)
        {
            var result = new RequestResult<Contact>();
            try
            {
                if (repo.GetById(contact.Id) == null)
                {
                    if(!UsernameTaken(contact.Username))
                    {
                        repo.Insert(contact, true);

                        //Consider creating a micro cache in REDIS for scale
                        //Or in the very least a seperate structure to hold cache values
                        if (!usernameCache.ContainsKey(contact.Id))
                        {
                            usernameCache.Add(contact.Id, contact.Username.ToLower());
                        }

                        result = RequestResultFactory.Generate<Contact>();
                        result.Results = new List<Contact>() { contact };
                    }
                    else
                    {
                        result = RequestResultFactory.GenerateConflict<Contact>("username");
                    }
                }
                else
                {
                    result = RequestResultFactory.GenerateConflict<Contact>();
                }
                
            }
            catch(Exception ex)
            {
                result = RequestResultFactory.GenerateException<Contact>(ex);
            }
            return result;
        }

        public RequestResult<Contact> Get(int page=0, int pageSize=10, bool includeDeleted=false)
        {
            return contactRepo.Get(page, pageSize, includeDeleted);
        }

        public RequestResult<Contact> Update(Guid id, Contact contact)
        {
            var result = new RequestResult<Contact>();
            try
            {
                var obj = repo.GetById(id);
                if(obj != null)
                {
                    if(!UsernameTaken(contact.Username))
                    {
                        repo.Update(obj, contact, true);
                        result = RequestResultFactory.Generate<Contact>();
                        result.Results = new List<Contact>() { obj };

                        //Don't forget to update the cache
                        usernameCache[id] = contact.Username;
                    }
                    else
                    {
                        result = RequestResultFactory.GenerateConflict<Contact>("username");                       
                    }
                }
                else
                {
                    result = RequestResultFactory.GenerateNotFound<Contact>();
                }
            }
            catch(Exception ex)
            {
                result = RequestResultFactory.GenerateException<Contact>(ex);
            }
            return result;
        }

        public RequestResult<Contact> Delete(Guid id, bool hardDelete=false)
        {
            RequestResult<Contact> result;
            try
            {
                var obj = repo.GetById(id);
                if(obj != null)
                {
                    if (!hardDelete)
                    {
                        repo.Update(obj, new Contact()
                        {
                             Username = obj.Username,
                             ContactStatus =  ContactStatus.DELETED,
                             ContactStatusMessage = "Contact Deleted - awaiting archival"
                        }, true);
                    }
                    else
                    {
                        repo.Delete(id, true);
                    }

                    if (usernameCache.ContainsKey(id)) usernameCache.Remove(id);

                    result = RequestResultFactory.Generate<Contact>();
                    result.Results = new List<Contact>() { obj };
                }
                else
                {
                    result = RequestResultFactory.GenerateNotFound<Contact>();
                }
            }
            catch(Exception ex)
            {
                result = RequestResultFactory.GenerateException<Contact>(ex);
            }
            return result;
        }

        private int UsernameTakenDatabase(string username)
        {
            var x = repo.GetAllQueryable();
            return repo.GetAllQueryable().Count(x => x.Username.ToLower() == username.ToLower());
        }

        private bool UsernameTaken(string username)
        {
            if(usernameCache.ContainsValue(username.ToLower()))
            {
                return true;
            }
            var c = UsernameTakenDatabase(username);
            return c > 0;
        }

        public Contact GetById(Guid Id)
        {
            return repo.GetById(Id);
        }

        public Guid GetIdForUser(string username)
        {
            if(usernameCache.ContainsValue(username))
            {
                return usernameCache.FirstOrDefault(x => x.Value == username.ToLower()).Key;
            }
            var user = repo.GetAll().FirstOrDefault(x => x.Username == username);
            if(user != null)
            {
                return user.Id;
            }
            return Guid.Empty;
        }
    }
}
