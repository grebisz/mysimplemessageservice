using System;

namespace Acme.MySimpleMessageService.Common.Enums
{
    public enum ContactStatus{
        ONLINE,
        OFFLINE,
        AWAY,
        DND,
        INVISIBLE,
        DELETED
    }
}