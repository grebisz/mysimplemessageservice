﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Common.Enums
{
    public enum MessageStatus
    {
        SENT,
        READ,
        DELETED,
        DEFAULT
    }
}
