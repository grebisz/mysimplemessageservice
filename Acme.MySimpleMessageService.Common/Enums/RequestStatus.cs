using System;

namespace Acme.MySimpleMessageService.Common.Enums
{
    //Status for requests
    public enum RequestStatus{
        SUCCESS,
        FAILURE,
        WARNING
    }
}