﻿using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Acme.MySimpleMessageService.Common
{
    public static class RequestResultFactory
    {
        public static RequestResult<T> GenerateException<T>(Exception ex, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError, RequestStatus status = RequestStatus.FAILURE)
        {
            return new RequestResult<T>()
            {
                HttpStatusCode = httpStatusCode,
                Status = status,
                Message = $"{ex.Message}\n{ex.StackTrace}",
                AdditionalInfo = ex.InnerException != null ? $"{ex.InnerException.Message}\n{ex.InnerException.StackTrace}" : ""
            };
        }

        public static RequestResult<T> Generate<T>(long totalCount=1, string message="", string additionalInfo="", HttpStatusCode httpStatusCode = HttpStatusCode.OK, RequestStatus status = RequestStatus.SUCCESS, List<T> objectList = null, int page=0, int pageSize=0)
        {
            //Dangerous cast if we approach the upper max of int
            if (pageSize < 1) pageSize = totalCount < 1 ? 1 : (int)totalCount;
            var result =  new RequestResult<T>()
            {
                HttpStatusCode = httpStatusCode,
                Status = status,
                Message = message,
                AdditionalInfo = additionalInfo,
                TotalCount = totalCount
            };
            if(objectList != null)
            {
                result.Results = objectList;
                result.Page = page;
                result.PageSize = pageSize;
                result.TotalPages = (int)Math.Floor((decimal)totalCount / (decimal)pageSize);
            }
            return result;
        }

        public static RequestResult<T> GenerateNotFound<T>()
        {
            return new RequestResult<T>()
            {
                HttpStatusCode = HttpStatusCode.NotFound,
                Results = new List<T>(),
                Status = RequestStatus.FAILURE
            };
        }

        public static RequestResult<T> GenerateConflict<T>(string property="id")
        {
            return new RequestResult<T>()
            {
                HttpStatusCode = HttpStatusCode.Conflict,
                Status = RequestStatus.FAILURE,
                Results = new List<T>(),
                Message = $"That {property} is already in use"
            };
        }

        public static RequestResult<T> GenerateArguementError<T>(string arguement, string error)
        {
            return new RequestResult<T>()
            {
                HttpStatusCode = HttpStatusCode.BadRequest,
                Status = RequestStatus.FAILURE,
                Results = new List<T>(),
                Message = $"Arguement {arguement} is invalid",
                AdditionalInfo = error
            };
        }
    }
}
