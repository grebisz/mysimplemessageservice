using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Acme.MySimpleMessageService.Common.Models
{
    public class Message
    {
        public Guid Id {get;set;}
        public Guid? SenderId {get;set;}

        [ForeignKey("SenderId")]
        public virtual Contact Sender {get;set;}
        public Guid? ReceiverId {get;set;}

        [ForeignKey("ReceiverId")]
        public virtual Contact Receiver {get;set;}
        public MessageStatus Status {get;set;}

        public string MessageText { get; set; }

        public DateTime DateSent { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}