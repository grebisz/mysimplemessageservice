using Acme.MySimpleMessageService.Common.Enums;
using System;

namespace Acme.MySimpleMessageService.Common.Models
{
    public class Contact
    {
        public Guid Id {get;set;}
        public string Username {get;set;}
        public ContactStatus ContactStatus {get;set;}
        public string ContactStatusMessage {get;set;}
    }
}