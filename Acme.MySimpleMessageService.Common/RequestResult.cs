using Acme.MySimpleMessageService.Common.Enums;
using System;
using System.Collections.Generic;
using System.Net;

namespace Acme.MySimpleMessageService.Common
{
    public class RequestResult<ReturnType>
    {
        public RequestStatus Status {get;set;}
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Message {get;set;}
        public string AdditionalInfo {get;set;}
        public List<ReturnType> Results {get;set;}
        public long TotalCount {get;set;}
        public int PageSize {get;set;}
        public int Page {get;set;}
        public int TotalPages {get;set;}
    }
}