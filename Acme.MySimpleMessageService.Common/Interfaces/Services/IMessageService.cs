﻿using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Common.Common.Interfaces.Services
{
    public interface IMessageService
    {
        void MarkMessageAsRead(Guid messageId);
        RequestResult<Message> SendMessage(string fromName, string toName, string message);
        RequestResult<Message> SendMessage(Guid fromUser, Guid toUser, string message);
        RequestResult<Message> SendMessage(Contact from, Contact to, string message);
        RequestResult<Message> DeleteMessage(Guid messageId, bool hardDelete = false);
        RequestResult<Message> GetMessages(Guid contactId,
            int page = 0, int pageSize = 10, MessageStatus messageStatus = MessageStatus.SENT,
            DateTime? fromDateFilter = null, DateTime? toDatefilter = null,
            string sortBy = "DateSent", bool sortAsc = false);
    }
}
