using System;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Models;

namespace Acme.MySimpleMessageService.Common.Interfaces
{
    public interface IContactService
    {
        Contact GetById(Guid Id);
        Guid GetIdForUser(string username);
    }
}