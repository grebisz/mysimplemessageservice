﻿using Acme.MySimpleMessageService.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Common.Interfaces.Repositories
{
    public interface IContactRepository
    {
        RequestResult<Contact> Get(int page = 0, int pageSize = 10, bool includeDeleted = false);
    }
}
