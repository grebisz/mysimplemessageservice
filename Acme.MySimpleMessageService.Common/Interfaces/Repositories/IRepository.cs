using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acme.MySimpleMessageService.Repositories;

namespace Acme.MySimpleMessageService.Repositories.Interfaces{
    public interface IRepository<IDType, ObjectType>{
        ObjectType GetById(IDType id);
        ObjectType Insert(ObjectType o, bool save=false);
        ObjectType Update(ObjectType dbEntity, ObjectType o, bool save=false);
        ObjectType Delete(IDType id, bool save=false);
        long Count();
        //Not Ideal, but due to time constraints, these are here
        IQueryable<ObjectType> Get(int page = 0, int pagesize = 10);
        IQueryable<ObjectType> GetAllQueryable();
        //Please only use the below for memory repository
        //This will select the whole databases messages.
        //No one wants that.
        List<ObjectType> GetAll();
    }
}