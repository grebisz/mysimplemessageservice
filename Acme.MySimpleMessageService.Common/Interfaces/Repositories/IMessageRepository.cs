﻿using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Common.Interfaces.Repositories
{
    public interface IMessageRepository
    {
        RequestResult<Message> GetMessages(Guid contactId,
            int page = 0, int pageSize = 10, MessageStatus messageStatus = MessageStatus.DEFAULT,
            DateTime? fromDateFilter = null, DateTime? toDatefilter = null,
            string sortBy = "DateSent", bool sortAsc = false);
    }
}
