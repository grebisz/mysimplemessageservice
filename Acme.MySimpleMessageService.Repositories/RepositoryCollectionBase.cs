﻿using Acme.MySimpleMessageService.Common.Models;
using System;

namespace Acme.MySimpleMessageService.Repositories.Interfaces
{
    public abstract class RepositoryCollectionBase
    {
        public IRepository<Guid, Contact> ContactRepository { get; protected set; }

        public IRepository<Guid, Message> MessageRepository { get; protected set; }
    }
}
