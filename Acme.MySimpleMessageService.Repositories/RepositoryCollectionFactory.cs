﻿using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Repositories
{
    public class RepositoryCollectionFactory
    {
        public static RepositoryCollectionBase GenerateEFRepository(MySimpleMessageServiceContext context)
        {
            return new EFRepositoryCollection(context);
        }

        public static RepositoryCollectionBase GenerateEFRepository(string configFilePath)
        {
            return new EFRepositoryCollection(configFilePath);
        }

        public static RepositoryCollectionBase GenerateMemoryRepositoryCollection()
        {
            return new MemoryRepositoryCollection();
        }
    }
}
