using System;
using System.Linq;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;
using Acme.MySimpleMessageService.Common.Models;

namespace Acme.MySimpleMessageService.Repositories.Memory{
    //If specialised repository functions are needed, they can be implemented here via interfaces
    public class MemoryContactRepository : BaseMemoryRepository<Guid, Contact>, IContactRepository
    {
        public MemoryContactRepository() : base()
        {
            
        }
        //Overriden Methods of Base Repository
        public override Contact Insert(Contact o, bool save=false)
        {
            o.Id = Guid.NewGuid();
            collection.Add(o.Id, o);
            return o;
        }
        public override Contact Update(Contact dbEntity, Contact o, bool save=false)
        {
            var obj = GetById(dbEntity.Id);
            obj.Username = o.Username;
            obj.ContactStatusMessage = o.ContactStatusMessage;
            obj.ContactStatus = o.ContactStatus;
            return obj;
        }
        public override Contact Delete(Guid id, bool save=false)
        {
            var obj = GetById(id);
            if(obj != null)
            {
                collection.Remove(id);
            }
            return obj;
        }

        public RequestResult<Contact> Get(int page = 0, int pageSize = 10, bool includeDeleted = false)
        {
            if (page < 0) page = 0;
            if (pageSize < 0) pageSize = Int32.MaxValue;
            var contactlist = collection.Values.ToList();
            var contacts = contactlist.Where(x => x.ContactStatus != Common.Enums.ContactStatus.DELETED).ToList();
            if (includeDeleted) contacts.AddRange(contactlist.Where(x => x.ContactStatus == Common.Enums.ContactStatus.DELETED).ToList());
            var count = contacts.Count;
            contacts = contacts.Skip(page * pageSize).Take(pageSize).ToList();
            return RequestResultFactory.Generate<Contact>(count, objectList: contacts, page: page, pageSize: pageSize);
        }
    }
}