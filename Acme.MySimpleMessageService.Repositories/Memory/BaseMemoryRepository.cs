﻿using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acme.MySimpleMessageService.Repositories.Memory
{
    public abstract class BaseMemoryRepository<IDType, OType> : IRepository<IDType, OType> where OType : class
    {
        protected Dictionary<IDType, OType> collection { get; set; }
        public BaseMemoryRepository()
        {
            this.collection = new Dictionary<IDType, OType>();
        }

        public long Count()
        {
            return collection.Count;
        }

        public OType GetById(IDType id)
        {
            return collection.ContainsKey(id) ? collection[id] : null;
        }

        public abstract OType Insert(OType o, bool save = false);

        public abstract OType Update(OType dbEntity, OType o, bool save = false);

        public abstract OType Delete(IDType id, bool save = false);

        public IQueryable<OType> GetAllQueryable()
        {
            //No queryables on memory repos
            throw new NotImplementedException();
        }
        public IQueryable<OType> Get(int page, int pagesize)
        {
            throw new NotImplementedException();
        }

        public List<OType> GetAll() 
        {
            return collection.Values.ToList();
        }
    }
}
