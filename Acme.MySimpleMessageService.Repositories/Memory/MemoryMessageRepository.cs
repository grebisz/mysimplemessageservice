using System;
using System.Linq;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;
using Acme.MySimpleMessageService.Common.Models;

namespace Acme.MySimpleMessageService.Repositories.Memory{
    //If specialised repository functions are needed, they can be implemented here via interfaces
    public class MemoryMessageRepository : BaseMemoryRepository<Guid, Message>, IMessageRepository
    {
        public MemoryMessageRepository() : base()
        {
            
        }
        public override Message Delete(Guid id, bool save = false)
        {
            var obj = GetById(id);
            if(obj != null)
            {
                collection.Remove(id);
            }
            return obj;
        }

        public RequestResult<Message> GetMessages(Guid contactId, int page = 0, int pageSize = 10, MessageStatus messageStatus = MessageStatus.DEFAULT, DateTime? fromDateFilter = null, DateTime? toDatefilter = null, string sortBy = "DateSent", bool sortAsc = false)
        {
            var messages = this.GetAll().Where(x => x.Id == contactId);
            if(messageStatus == MessageStatus.DEFAULT)
            {
                messages = messages.Where(x => x.Status != MessageStatus.DELETED).ToList();
            }
            if(fromDateFilter.HasValue && toDatefilter.HasValue)
            {
                messages = messages.Where(x => x.DateSent >= fromDateFilter.Value).Where(x => x.DateSent < toDatefilter.Value.AddDays(1)).ToList();
            }
            return RequestResultFactory.Generate<Message>(messages.Count(), httpStatusCode: System.Net.HttpStatusCode.OK, status: RequestStatus.SUCCESS, objectList: messages.Skip(page * pageSize).Take(pageSize).ToList());
        }

        public override Message Insert(Message o, bool save = false)
        {
            o.Id = Guid.NewGuid();
            collection.Add(o.Id, o);
            return o;
        }

        public override Message Update(Message dbEntity, Message o, bool save = false)
        {
            var obj = GetById(dbEntity.Id);
            obj.MessageText = o.MessageText;
            obj.Receiver = o.Receiver;
            obj.ReceiverId = o.ReceiverId;
            obj.Sender = o.Sender;
            obj.Status = o.Status;
            obj.DateSent = o.DateSent;
            obj.DateRead = o.DateRead;
            obj.DateDeleted = o.DateDeleted;
            return dbEntity;
        }
    }
}