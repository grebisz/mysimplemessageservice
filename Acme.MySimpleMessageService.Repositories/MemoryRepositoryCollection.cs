﻿using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Repositories
{
    public class MemoryRepositoryCollection : RepositoryCollectionBase
    {
        public MemoryRepositoryCollection()
        {
            ContactRepository = new Memory.MemoryContactRepository();
            MessageRepository = new Memory.MemoryMessageRepository();
        }
    }
}
