using System;
using System.Linq;
using System.Threading.Tasks;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Acme.MySimpleMessageService.Repositories.EF{
    //If specialised repository functions are needed, they can be implemented here via interfaces
    public class EFContactRepository : BaseEFRepository<Guid, Contact>, IContactRepository
    {
        public EFContactRepository(MySimpleMessageServiceContext context) : base(context, context.Contacts)
        {
            
        }
        //Overriden Methods of Base Repository
        public override Contact GetById(Guid id)
        {
            return databaseSet.FirstOrDefault(x => x.Id == id);
        }
        public override Contact Insert(Contact o, bool save=false)
        {
            databaseSet.Add(o);
            if(save) context.SaveChanges();
            return o;
        }
        public override Contact Update(Contact dbEntity, Contact o, bool save=false)
        {
            dbEntity.Username = o.Username;
            dbEntity.ContactStatusMessage = o.ContactStatusMessage;
            dbEntity.ContactStatus = o.ContactStatus;
            databaseSet.Update(dbEntity);
            if(save) context.SaveChanges();
            return dbEntity;
        }
        public override Contact Delete(Guid id, bool save=false)
        {
            var obj = GetById(id);
            if(obj.Id == id)
            {
                databaseSet.Remove(obj);
                if(save) context.SaveChanges();
            }
            return obj;
        }

        public RequestResult<Contact> Get(int page = 0, int pageSize = 10, bool includeDeleted = false)
        {
            var query = GetAllQueryable();
            if (!includeDeleted) query = query.Where(x => x.ContactStatus != Common.Enums.ContactStatus.DELETED);
            var list = query.ToList();
            return RequestResultFactory.Generate<Contact>(list.Count, objectList: list, page: page, pageSize: pageSize);
        }
    }
}