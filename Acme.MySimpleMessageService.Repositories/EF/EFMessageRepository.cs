using System;
using System.Linq;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.ExtensionMethods;
using Acme.MySimpleMessageService.Common.Interfaces.Repositories;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Acme.MySimpleMessageService.Repositories.EF{
    //If specialised repository functions are needed, they can be implemented here via interfaces
    public class EFMessageRepository : BaseEFRepository<Guid, Message>, IMessageRepository
    {
        IRepository<Guid, Contact> ContactRepo;
        
        public EFMessageRepository(MySimpleMessageServiceContext context) : base(context, context.Messages)
        {
            ContactRepo = new EFContactRepository(context);
        }
        public override Message Delete(Guid id, bool save = false)
        {
            var obj = GetById(id);
            databaseSet.Remove(obj);
            if(save) context.SaveChanges();
            return obj;
        }
        public override Message GetById(Guid id)
        {
            return databaseSet.FirstOrDefault(x => x.Id == id);
        }

        public RequestResult<Message> GetMessages(Guid contactId, int page = 0, int pageSize = 10, MessageStatus messageStatus = MessageStatus.DEFAULT, DateTime? fromDateFilter = null, DateTime? toDatefilter = null, string sortBy = "DateSent", bool sortAsc = false)
        {
            var result = new RequestResult<Message>();
            try
            {
                var contact = ContactRepo.GetById(contactId);
                if (contact != null)
                {
                    //Fetch all messages for the current user
                    var contactList = GetAllQueryable().Where(x => x.ReceiverId == contactId);
                    //If we dont include deleted, make sure to exclude results where the status is deleted
                    //Date filtration
                    if (fromDateFilter.HasValue) contactList = contactList.Where(x =>
                            x.DateSent >= fromDateFilter.Value);

                    if (toDatefilter.HasValue) contactList = contactList.Where(x =>
                            x.DateSent <= toDatefilter.Value.AddDays(1)
                    );

                    if (messageStatus != MessageStatus.DEFAULT)
                    {
                        contactList = contactList.Where(x => x.Status == messageStatus);
                    }
                    else
                    {
                        contactList = contactList.Where(x => x.Status != MessageStatus.DELETED);
                    }

                    //Order the query according to parameters
                    IOrderedQueryable<Message> orderedQuery;
                    if (sortAsc)
                    {
                        orderedQuery = contactList.OrderBy<Message>(sortBy);
                    }
                    else
                    {
                        orderedQuery = contactList.OrderByDescending<Message>(sortBy);
                    }
                    result = RequestResultFactory.Generate<Message>(totalCount: orderedQuery.Count(), objectList: orderedQuery.Include(x => x.Sender).Skip(page * pageSize).Take(pageSize).ToList(), page: page, pageSize: pageSize);
                }
            }
            catch (Exception ex)
            {
                result = RequestResultFactory.GenerateException<Message>(ex);
            }
            return result;
        }

        public override Message Insert(Message o, bool save = false)
        {
            databaseSet.Add(o);
            if(save) context.SaveChanges();
            return o;
        }
        public override Message Update(Message dbEntity, Message o, bool save = false)
        {
            dbEntity.ReceiverId = o.ReceiverId;
            dbEntity.SenderId = o.SenderId;
            dbEntity.Status = o.Status;
            dbEntity.DateDeleted = o.DateDeleted;
            dbEntity.DateRead = o.DateRead;
            dbEntity.DateSent = o.DateSent;
            databaseSet.Update(dbEntity);
            if(save) context.SaveChanges();
            return dbEntity;
        }
    }
}