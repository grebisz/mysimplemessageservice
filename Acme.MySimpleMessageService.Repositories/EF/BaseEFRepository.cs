﻿using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acme.MySimpleMessageService.Repositories.EF
{
    public abstract class BaseEFRepository<IDType, OType> : IRepository<IDType, OType> where OType : class
    {
        protected MySimpleMessageServiceContext context { get; set; }
        protected DbSet<OType> databaseSet { get; set; }
        public BaseEFRepository(MySimpleMessageServiceContext context, DbSet<OType> dbSet)
        {
            this.context = context;
            this.databaseSet = dbSet;
        }

        public long Count()
        {
            return databaseSet.Count();
        }

        public IQueryable<OType> Get(int page = 0, int pagesize = 10)
        {
            return databaseSet.Skip(page * pagesize).Take(pagesize);
        }

        public abstract OType GetById(IDType id);

        public abstract OType Insert(OType o, bool save = false);

        public abstract OType Update(OType dbEntity, OType o, bool save = false);

        public abstract OType Delete(IDType id, bool save = false);

        public IQueryable<OType> GetAllQueryable()
        {
            return databaseSet.AsQueryable<OType>();
        }

        public List<OType> GetAll()
        {
            //This part of the interface is for memory only.
            throw new NotImplementedException();
        }
    }
}
