﻿using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Repositories
{
    public class EFRepositoryCollection : RepositoryCollectionBase
    {
        public EFRepositoryCollection(string filename="appsettings.json")
        {
            if(System.IO.File.Exists(filename))
            {
                var config = new ConfigurationBuilder()
                    .AddJsonFile(filename)
                    .Build();

                string connectionStr = config["ConnectionStrings:db"];
                var contextOptions = new DbContextOptionsBuilder().UseSqlServer(connectionStr).Options;
                var context = new EntityFramework.MySimpleMessageServiceContext(contextOptions);
                Init(context);
            }
            else
            {
                throw new System.IO.IOException(filename + " - not found");
            }
            
        }

        public EFRepositoryCollection(MySimpleMessageServiceContext context)
        {
            Init(context);
        }

        public void Init(MySimpleMessageServiceContext context)
        {
            ContactRepository = new EF.EFContactRepository(context);
            MessageRepository = new EF.EFMessageRepository(context);
        }
    }
}
