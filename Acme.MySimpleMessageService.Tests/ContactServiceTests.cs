using System;
using System.Linq;
using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Tests.ObjectFactories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acme.MySimpleMessageService.Tests
{
    [TestClass]
    public class ContactServiceTests
    {
        private static ContactService contactService {get;set;}

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            ServiceCollection collection = new ServiceCollection();
            contactService = collection.ContactService;
        }

        [ClassCleanup]
        public static void CleanUp()
        {
            var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
            string connectionStr = config["ConnectionStrings:db"];
            var contextOptions = new DbContextOptionsBuilder().UseSqlServer(connectionStr).Options;
            var efcontext = new EntityFramework.MySimpleMessageServiceContext(contextOptions);
            var testusernames = new string[] { "TEST_Create", "TEST_Create_Duplicate", "TEST_Update_Post", "TEST_Update_Duplicate", "Test_Update_Duplicate_Target", "TEST_Delete", "TEST_HardDelete" };
            efcontext.Contacts.RemoveRange(efcontext.Contacts.Where(x => testusernames.Contains(x.Username.ToLower())));
            efcontext.SaveChanges();
        }

        [TestMethod]
        public void Create()
        {
            var result = contactService.Create(ContactFactory.GenerateContact("TEST_Create"));

            Assert.IsTrue(result.Results.Count == 1);
            Assert.AreEqual("TEST_Create", result.Results.FirstOrDefault().Username);
            Assert.IsTrue(result.Results[0].Id != Guid.Empty);
            Assert.IsTrue(result.Status == RequestStatus.SUCCESS);
        }

        [TestMethod]
        public void Create_Duplicate()
        {
            var o = ContactFactory.GenerateContact("TEST_Create_Duplicate");
            var p = ContactFactory.GenerateContact("TEST_Create_Duplicate");
            contactService.Create(o);
            var result = contactService.Create(p);

            Assert.IsTrue(result.Results.Count == 0);
            Assert.IsTrue(result.HttpStatusCode == System.Net.HttpStatusCode.Conflict);
            Assert.IsTrue(result.Status == RequestStatus.FAILURE);
        }

        [TestMethod]
        public void Update()
        {
            var o = ContactFactory.GenerateContact("TEST_Update_Pre");
            contactService.Create(o);
            var result = contactService.Update(o.Id, new Contact()
            {
                Username = "TEST_Update_Post",
                ContactStatus = ContactStatus.DND,
                ContactStatusMessage = "Updated to Do not Disturb"
            });

            Assert.IsTrue(result.Results.Count == 1);
            Assert.AreEqual(result.Results[0].Username, "TEST_Update_Post");
            Assert.IsTrue(result.Status == RequestStatus.SUCCESS);
        }

        [TestMethod]
        public void Update_Duplicate()
        {
            var o = contactService.Create(ContactFactory.GenerateContact("TEST_Update_Duplicate"));
            var p = contactService.Create(ContactFactory.GenerateContact("Test_Update_Duplicate_Target"));
            var result = contactService.Update(p.Results[0].Id, new Contact()
            {
                Username = "TEST_Update_Duplicate",
                ContactStatus = ContactStatus.DND,
                ContactStatusMessage = "Update to Do not Disturb"
            });

            Assert.IsTrue(result.Results.Count == 0);
            Assert.IsTrue(result.HttpStatusCode == System.Net.HttpStatusCode.Conflict);
            Assert.IsTrue(result.Status == RequestStatus.FAILURE);
        }

        [TestMethod]
        public void SoftDelete()
        {
            var o = contactService.Create(ContactFactory.GenerateContact("TEST_Delete"));
            var result = contactService.Delete(o.Results[0].Id);

            Assert.IsTrue(result.Results.Count == 1);
            Assert.IsTrue(result.HttpStatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsTrue(result.Status == RequestStatus.SUCCESS);

            //Check that this user is off of lists

            Assert.IsTrue(contactService.Get(-1, -1).Results.Where(x => x.Username == "TEST_Delete").Count() == 0);
            Assert.IsTrue(contactService.Get(-1, -1, true).Results.Where(x => x.Username == "TEST_Delete").Count() == 1);
        }

        [TestMethod]
        public void HardDelete()
        {
            var o = contactService.Create(ContactFactory.GenerateContact("TEST_HardDelete"));
            var result = contactService.Delete(o.Results[0].Id, true);

            Assert.IsTrue(result.Results.Count == 1);
            Assert.IsTrue(result.HttpStatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsTrue(result.Status == RequestStatus.SUCCESS);

            //Ensure ACTUAL deletion

            
            Assert.IsTrue(contactService.Get(-1, -1).Results.Where(x => x.Username == "TEST_HardDelete").Count() == 0);
            Assert.IsTrue(contactService.Get(-1, -1, true).Results.Where(x => x.Username == "TEST_HardDelete").Count() == 0);
        }
    }
}
