﻿using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.MySimpleMessageService.Tests.ObjectFactories
{
    public class ContactFactory
    {
        public static Contact GenerateContact(string username, ContactStatus status = ContactStatus.ONLINE, string statusM = "")
        {
            return new Contact()
            {
                Username = username,
                ContactStatus = status,
                ContactStatusMessage = statusM
            };
        }
    }
}
