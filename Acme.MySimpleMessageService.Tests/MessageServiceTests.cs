﻿using Acme.MySimpleMessageService.Common.Enums;
using Acme.MySimpleMessageService.Common.Models;
using Acme.MySimpleMessageService.EntityFramework;
using Acme.MySimpleMessageService.Common;
using Acme.MySimpleMessageService.Tests.ObjectFactories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acme.MySimpleMessageService.Tests
{
    [TestClass]
    public class MessageServiceTests
    {
        private static MessageService messageService { get; set; }

        private static Contact contact1 { get; set; }
        private static Contact contact2 { get; set; }

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            ServiceCollection collection = new ServiceCollection();
            messageService = collection.MessageService;
            contact1 = collection.ContactService.Create(ContactFactory.GenerateContact("MessageTest_Contact1")).Results.FirstOrDefault();
            contact2 = collection.ContactService.Create(ContactFactory.GenerateContact("MessageTest_Contact2")).Results.FirstOrDefault();
        }

        [TestMethod]
        public void SendMessage()
        {
            var result = messageService.SendMessage(contact1, contact2, "This is a test");

            Assert.IsTrue(result.Status == RequestStatus.SUCCESS);
            Assert.IsTrue(result.HttpStatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsTrue(result.Results[0].Id != Guid.Empty);

            var messages = messageService.GetMessages(contact2.Id);

            Assert.IsTrue(messages.TotalCount == 1);
            Assert.IsTrue(messages.Results.Count == 1);
            Assert.IsTrue(messages.Results[0].Id == result.Results[0].Id);
        }

        [TestMethod]
        public void TestMarkAsRead()
        {
            messageService.SendMessage(contact2, contact1, "MarkAsReadTest");

            var messages = messageService.GetMessages(contact1.Id);
            Assert.IsTrue(messages.Results[0].Status == MessageStatus.SENT);
            messageService.MarkMessageAsRead(messages.Results[0].Id);
            Assert.IsTrue(messages.Results[0].Status == MessageStatus.READ);

            messages = messageService.GetMessages(contact1.Id, messageStatus: MessageStatus.SENT);
            Assert.IsTrue(messages.Results.Count == 0);
        }

        [TestMethod]
        public void HardDelete()
        {
            var message = messageService.SendMessage(contact2, contact1, "A very incriminating message.").Results[0];
            messageService.DeleteMessage(message.Id, true);

            var messages = messageService.GetMessages(message.Id);

            Assert.IsNull(messages.Results);
        }

        [TestMethod]
        public void SoftDelete()
        {
            var message = messageService.SendMessage(contact2, contact1, "A very incriminating message - that will be discovered.").Results[0];
            messageService.DeleteMessage(message.Id);
            var messages = messageService.GetMessages(contact1.Id, messageStatus: MessageStatus.DEFAULT);
            Assert.IsTrue(!messages.Results.Select(x => x.Id).Contains(message.Id));
            Assert.IsTrue(message.Status == MessageStatus.DELETED);
        }

        [ClassCleanup]
        public static void CleanUp()
        {
            var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
            string connectionStr = config["ConnectionStrings:db"];
            var contextOptions = new DbContextOptionsBuilder().UseSqlServer(connectionStr).Options;
            var efcontext = new EntityFramework.MySimpleMessageServiceContext(contextOptions);

            var contactGuids = new List<Guid> { contact1.Id, contact2.Id };

            var testusernames = new string[] { "MessageTest_Contact1", "MessageTest_Contact2" };
            efcontext.Messages.RemoveRange(efcontext.Messages.Where(x => contactGuids.Contains(x.SenderId.Value) || contactGuids.Contains(x.ReceiverId.Value)));
            efcontext.Contacts.RemoveRange(efcontext.Contacts.Where(x => testusernames.Contains(x.Username.ToLower())));
            efcontext.SaveChanges();
        }
    }
}
