﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Acme.MySimpleMessageService.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace Acme.MySimpleMessageService.EntityFramework
{
    public class MySimpleMessageServiceContext : DbContext
    {
        public DbSet<Contact> Contacts {get;set;}
        public DbSet<Message> Messages {get;set;}
        public MySimpleMessageServiceContext(DbContextOptions options) : base(options)
        {

        }
    }
}
